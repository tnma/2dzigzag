# initialize some variables
size = int(raw_input('Please enter the matrix size: '))
matrix = [[0 for x in range(size)] for y in range(size)]
length = 1
right = False
x = 0
y = 0
# loop to generate the first half of the matrix
while length <= ((size * size) / 2) + 1:
    matrix[x][y] = length
    if right:
        if (x + 1) >= size:
            right = False
        else:
            x += 1
        if (y - 1) < 0:
            right = False
        else:
            y -= 1
    else:
        if (x - 1) < 0:
            right = True
        else:
            x -= 1
        if (y + 1) >= size:
            right = True
        else:
            y += 1
    length += 1
# fill in the missing half and print the matrix
print('Result: ')
for x in range(size):
    line = ''
    for y in range(size):
        if matrix[x][y] == 0:
            matrix[x][y] = (size * size) + 1 - matrix[size - x - 1][size - y - 1]
        line += str(matrix[x][y]).ljust(len(str((size * size) - 1)) + 1)
    print(line)
